﻿using UnityEngine;
using System.Collections;

public class FinishTestLevelBox : MonoBehaviour {

	private Player player;

	void Awake() {
		player = FindObjectOfType<Player>();
	}

	void OnTriggerEnter2D(Collider2D col) {
		player.transform.position = player.respawnPosition;
	}
}
