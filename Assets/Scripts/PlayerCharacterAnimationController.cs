﻿using UnityEngine;
using System.Collections;

public class PlayerCharacterAnimationController : MonoBehaviour {

	public Player player;
	public Animator animator;
	public Transform aimingArm;
	public Transform beamPosition;

	public float turnSpeed = 10f;
	private GrappleBeam grappleBeam;
	private bool facingRight = true;

	void Awake() {
		grappleBeam = player.GetComponent<GrappleBeam>();
	}

	void Update() {
		animator.SetBool("Running", Input.GetAxis("Horizontal") != 0);
		if(Input.GetAxis("Horizontal") != 0)
			facingRight = Input.GetAxis("Horizontal") < 0;

		Vector2 aimArmPoint = (grappleBeam.grappled) ? grappleBeam.grappleJoint.connectedAnchor : ArmCannonController.GetMousePosition();
		float AngleRad = Mathf.Atan2(aimArmPoint.y - aimingArm.position.y, aimArmPoint.x - aimingArm.position.x);
		float AngleDeg = (180 / Mathf.PI) * AngleRad;
		aimingArm.rotation = Quaternion.Euler(0, 0, AngleDeg + 90);

		float direction = facingRight ? 180 : 0;
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, direction, 0), Time.deltaTime * turnSpeed);
	}
}
