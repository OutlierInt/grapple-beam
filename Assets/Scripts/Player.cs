﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour {
	private Rigidbody2D playerRigidbody;
	public SpriteRenderer jetPackSpriteRenderer;
	[HideInInspector]
	public PlayerCharacterAnimationController animationController;

	public float jumpForce = 20;
	public float jetPackForce = 30;
	public bool jetPackActive = false;
	public float jetPackFuelTotal = 2;
	public float jetPackFuelAvailable = 2;
	public float jetPackFuelRechargeRate = 0.5f;
	public float jetPackFuelRechargeWaitTime = 0.5f;
	private float jetPackFuelRechargeWaitTimer = 0;
	public ParticleSystem jetpackEffect;
	public Color[] jetpackEffectColors;
	public Color[] playerJetPackFuelColors;

	public float groundMoveSpeed = 5;
	public float aerialMoveSpeed = 5;
	public float grapplePushForce = 5;
	public GrappleBeam grappleBeam;
	public LayerMask groundLayer;
	public float groundCheckDistance = 0.6f;
	public float groundCheckSkinWidth = 0.01f;
	public bool grounded = false;
	public Vector3 respawnPosition = new Vector3(-8,3,0);

	public float timeControlSpeed = 0.1f;
	public float timeControlChangeSpeed = 1f;
	public KeyCode timeControlKey = KeyCode.LeftControl;
	private float defaultFixedTimeScale;
	private float chosenTimeScale, chosenFixedDeltaTime;

	void Awake() {
		playerRigidbody = GetComponent<Rigidbody2D>();
		if(jetPackSpriteRenderer == null)
			jetPackSpriteRenderer = GetComponent<SpriteRenderer>();
		defaultFixedTimeScale = Time.fixedDeltaTime;
		animationController = GetComponentInChildren<PlayerCharacterAnimationController>();
	}

	void Update() {
		if(Input.GetButtonDown("Jump")) {
			if(grounded) {
				playerRigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
				//playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, jumpForce);
				transform.Translate(Vector3.up * 0.01f, Space.Self);
				grounded = false;
			}
			else {
				jetPackActive = true;
			}
		}

		//Restart if you fall out of bounds
		if(transform.position.y < -15) {
			transform.position = respawnPosition;
			playerRigidbody.velocity = Vector2.zero;
			grappleBeam.releaseGrapple();
		}

		//Time Control
		chosenTimeScale = Input.GetKey(timeControlKey) ? timeControlSpeed : 1;
		chosenFixedDeltaTime = Input.GetKey(timeControlKey) ? defaultFixedTimeScale * timeControlSpeed : defaultFixedTimeScale;

		Time.timeScale = Mathf.Lerp(Time.timeScale, chosenTimeScale, timeControlChangeSpeed * Time.deltaTime * 1f/Time.timeScale);
		Time.fixedDeltaTime = Mathf.Lerp(Time.fixedDeltaTime, chosenFixedDeltaTime, timeControlChangeSpeed * Time.deltaTime * 1f/Time.timeScale);

		//Jetpack Effect Color
		jetpackEffect.startColor = Color.Lerp(jetpackEffectColors[0], jetpackEffectColors[1], jetPackFuelAvailable/jetPackFuelTotal);
		jetPackSpriteRenderer.color = Color.Lerp(playerJetPackFuelColors[0], playerJetPackFuelColors[1], jetPackFuelAvailable/jetPackFuelTotal);

	}

	void FixedUpdate () {
		grounded = checkIfGrounded();
		Debug.DrawLine(transform.position, transform.position + (Vector3.down * groundCheckDistance), Color.red);

		if(grounded) {
			jetPackActive = false;
			jetPackFuelAvailable = jetPackFuelTotal;
			jetpackEffect.enableEmission = false;
		}

		float moveInput = Input.GetAxis("Horizontal");
		if(moveInput != 0) {
			if(!grappleBeam.grappled) {
				//Ground Movement or Air Movement
				if(grounded)
					playerRigidbody.velocity = new Vector2(groundMoveSpeed * moveInput, playerRigidbody.velocity.y);
				else
					playerRigidbody.AddForce(Vector2.right * aerialMoveSpeed * moveInput);
			}
			//Grapple Push Force
			else
				playerRigidbody.AddForce(Vector2.right * grapplePushForce * moveInput);
		}

		//Jet pack
		if(Input.GetButton("Jump") && !grounded && jetPackActive) {
			if(jetPackFuelAvailable > 0) {
				playerRigidbody.AddForce(Vector2.up * jetPackForce, ForceMode2D.Force);
				jetPackFuelAvailable -= Time.fixedDeltaTime;
				jetPackFuelRechargeWaitTimer = jetPackFuelRechargeWaitTime;
				jetpackEffect.enableEmission = true;
			}
		}

		jetpackEffect.enableEmission = Input.GetButton("Jump") && jetPackFuelAvailable > 0 && !grounded && jetPackActive;

		if(!grounded) {
			if(jetPackFuelRechargeWaitTimer > 0)
				jetPackFuelRechargeWaitTimer -= Time.fixedDeltaTime;
			else {
				jetPackFuelAvailable += jetPackFuelRechargeRate * Time.fixedDeltaTime;
				jetPackFuelAvailable = Mathf.Min(jetPackFuelAvailable, jetPackFuelTotal);
			}
		}
	}

	public bool checkIfGrounded() {
		Vector2 playerPos = transform.position;
		Vector2 positionCheck = Vector2.right * (transform.localScale.x / 2f - groundCheckSkinWidth);
		for (int i = -1; i < 2; i++) {
			bool hit = Physics2D.Raycast(playerPos + (positionCheck * i), Vector2.down, groundCheckDistance, groundLayer);
			Debug.DrawLine(playerPos + (positionCheck * i), (Vector3)(playerPos + (positionCheck * i)) + (Vector3.down * groundCheckDistance), Color.red);
			if(hit)
				return true;
		}
		return false;
	}
}
