﻿using UnityEngine;
using System.Collections;

public class ArmCannonController : MonoBehaviour {

	[HideInInspector]
	public ArmCannonGun gun;
	public Transform armCannonOrigin;

	void Awake() {
		gun = GetComponent<ArmCannonGun>();
	}

	public static Vector2 GetMousePosition() {
		return Camera.main.ScreenToWorldPoint(Input.mousePosition);
	}

	public Quaternion GetAimRotation() {
		Vector2 aimArmPoint = GetMousePosition();
		float AngleRad = Mathf.Atan2(aimArmPoint.y - armCannonOrigin.position.y, aimArmPoint.x - armCannonOrigin.position.x);
		float AngleDeg = (180 / Mathf.PI) * AngleRad;
		return Quaternion.Euler(0, 0, AngleDeg - 90);
	}
}
