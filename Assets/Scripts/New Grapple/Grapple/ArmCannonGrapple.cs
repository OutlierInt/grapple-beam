﻿using UnityEngine;
using System.Collections;

public class ArmCannonGrapple : MonoBehaviour {

	ArmCannonController manager;
	public GameObject grappleBeamVisual;
	public GrappleBeam grappler;

	public float maxDistance = 8;
	public float beamSpeed = 20;

	void Awake() {
		manager = GetComponent<ArmCannonController>();
	}
	
	void Update() {
		if(Input.GetMouseButtonDown(0)) {
			StopCoroutine("fireGrappleBeamProjectile");
			StartCoroutine(fireGrappleBeamProjectile());
		}
	}

	public IEnumerator fireGrappleBeamProjectile() {
		float distance = 0;
		float holdTime = 0.1f;
		while (distance < maxDistance || holdTime > 0) {
			distance += Time.deltaTime * beamSpeed;
			distance = Mathf.Min(distance, maxDistance);

			grappleBeamVisual.transform.position = manager.armCannonOrigin.position;
			grappleBeamVisual.transform.rotation = manager.GetAimRotation();
			grappleBeamVisual.transform.localScale = new Vector3(0.1f, distance, 1);

			if(distance >= maxDistance) {
				holdTime -= Time.deltaTime;
			}

			if(Physics2D.Raycast(grappleBeamVisual.transform.position, grappleBeamVisual.transform.up, distance, grappler.grappleMask)) {
				grappler.fireGrappleBeam(GrappleBeamType.Grapple);
				break;
			}

			yield return new WaitForEndOfFrame();
		}

		grappleBeamVisual.transform.localScale = new Vector3(0.1f, 0f, 1);
	}
}
