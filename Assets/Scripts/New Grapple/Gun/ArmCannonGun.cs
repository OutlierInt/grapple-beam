﻿using UnityEngine;
using System.Collections;

public class ArmCannonGun : MonoBehaviour {

	ArmCannonController manager;
	public GameObject bullet;
	public float fireRate = 0.125f;
	private float fireRateTimer = 0;

	void Awake() {
		manager = GetComponent<ArmCannonController>();
	}

	void Update() {
		if(Input.GetMouseButton(0)) {
			if(fireRateTimer <= 0) {
				FireBullet(manager.armCannonOrigin.position - manager.armCannonOrigin.up*0.5f, manager.GetAimRotation());
				fireRateTimer = fireRate;
			}
		}

		if(fireRateTimer > 0)
			fireRateTimer -= Time.deltaTime;
	}

	public void FireBullet(Vector3 position, Quaternion aimRotation) {
		GameObject bulletObj = Instantiate(bullet);
		bulletObj.transform.position = position;
		bulletObj.transform.rotation = aimRotation;
	}

}
