﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float speed = 20;
	public float lifeTime = 2;
	void Awake() {
		Destroy(gameObject, lifeTime);
	}

	void Update() {
		transform.position += transform.up * Time.deltaTime * speed;
	}

	void OnCollisionEnter2D( Collision2D col ) {
		Destroy(gameObject);
	}

}
