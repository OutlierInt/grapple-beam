﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum GrappleBeamType { Grapple, Gravity }
public class GrappleBeam : MonoBehaviour {

	public Player playerCon;
	public Rigidbody2D player;
	public DistanceJoint2D grappleJoint;
	public bool grappled = false;
	public float grappleBeamRange = 5;
	public float grappleExtendAmount = 0.1f;

	public GrappleBeamType grappleBeamCurType = GrappleBeamType.Grapple;
	public float gravityBeamRange = 10;
	public float gravityBeamRetractSpeed = 1;
	public float gravityBeamMinConvertDistance = 2;

	public LayerMask grappleMask;
	public Transform mousePositionUI;
	public Transform mouseAimLine;
	public Color aimColor;
	public Color outOfRangeAimColor;
	public float aimLineSize = 0.2f;
	public SpriteRenderer aimLineRenderer;

	public Color grappleBeamColor = Color.green;
	public Color gravityBeamColor = Color.blue;

	public bool grappleBeamCanBeWound = true;
	public List<Vector2> grappleBeamWindPoints = new List<Vector2>();
	public List<Vector2> grappleBeamWindPointNormals = new List<Vector2>();
	public float grappleBeamWindCheckLengthDecrease = 0.1f;

	public Vector3 gravityBeamScale = Vector3.one;
	private Vector3 normalPlayerScale;

	public GameObject grappleProjectileObject;

	public bool canFireBeamNormally = true;

	void Awake() {
		normalPlayerScale = transform.localScale;
	}

	void Update () {
		Vector2 playerPos = (Vector2)player.transform.position;

		//Attach Grapple Beam on mouse click
		if (Input.GetMouseButtonDown(0) && canFireBeamNormally)
			fireGrappleBeam(GrappleBeamType.Grapple);
		
		//Attach Gravity Beam on right click
		if(Input.GetMouseButtonDown(1) && canFireBeamNormally) {
			fireGrappleBeam(GrappleBeamType.Gravity);
			if(playerCon.grounded) {
				playerCon.grounded = false;
				player.transform.Translate(Vector3.up * 0.01f, Space.Self);
			}
		}

		//Release the Beam when mouse button released
		if(Input.GetMouseButtonUp(0))
			releaseGrapple();

		//Release the Gravity Beam when mouse released. Launch forward a bit.
		if(Input.GetMouseButtonUp(1)) {
			if(grappled && grappleBeamCurType == GrappleBeamType.Gravity)
				applyGravityBeamMomentumForce();

			releaseGrapple();
		}

		//When the player's feet are on the ground, release the grapple
		if(grappled && playerCon.grounded)
			releaseGrapple();

		//Shrink Player while riding gravity beam
		transform.localScale = (grappled && grappleBeamCurType == GrappleBeamType.Gravity) ? gravityBeamScale : normalPlayerScale;

		//Extend/Retract Grapple Beam
		float vInput = Input.GetAxis("Vertical");
		if(vInput != 0 && grappled) {
			grappleJoint.distance -= grappleExtendAmount * vInput * Time.deltaTime;
		}

		//Show Grapple Joint
		if(grappled)
			Debug.DrawLine(playerPos, grappleJoint.connectedAnchor, getGrappleBeamColor(grappleBeamCurType));

	}

	void FixedUpdate() {
		Vector2 playerPos = (Vector2)player.transform.position;

		//Allow the beam to wind/unwind around objects
		if(grappleBeamCanBeWound)
			grappleBeamWindingCheck(playerPos);

		//While in gravity beam mode, pull the player toward the grapple point
		if(grappled && grappleBeamCurType == GrappleBeamType.Gravity) {
			player.velocity = Vector2.zero;
			grappleJoint.distance -= gravityBeamRetractSpeed * Time.fixedDeltaTime;
			if(grappleJoint.distance < gravityBeamMinConvertDistance) {
				grappleBeamCurType = GrappleBeamType.Grapple;
				applyGravityBeamMomentumForce();
			}
		}
	}

	void LateUpdate() {
		Vector2 playerPos = playerCon.animationController.beamPosition.position; //(Vector2)player.transform.position;

		//Show Mouse Position
		Vector2 mousePosition = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mousePositionUI.transform.position = mousePosition;

		//Show Aim Line
		aimLineRenderer.enabled = !grappled;
		if (!grappled) {
			Vector2 mousePosToPlayerDirection = mousePosition - playerPos;
			float mousePosToPlayerDistance = Mathf.Min(mousePosToPlayerDirection.magnitude, grappleBeamRange);
			mouseAimLine.transform.position = Vector2.Lerp(playerPos, playerPos + (mousePosToPlayerDirection.normalized * mousePosToPlayerDistance), 0.5f);
			mouseAimLine.transform.localScale = new Vector3(mousePosToPlayerDistance, aimLineSize, aimLineSize);
			Color aimLineBeamColor = mousePosToPlayerDistance < grappleBeamRange ? aimColor : outOfRangeAimColor;
			aimLineRenderer.color = aimLineBeamColor;

			Vector3 vectorToTarget = mousePosition - playerPos;
			float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
			Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
			mouseAimLine.transform.rotation = q;
		}

		//Show Aim Direction
		if(!grappled) {
			Vector2 mousePosToPlayerDirection = mousePosition - playerPos;
			float mousePosToPlayerDistance = Mathf.Min(mousePosToPlayerDirection.magnitude, grappleBeamRange);
			Color debugAimColor = mousePosToPlayerDistance < grappleBeamRange ? aimColor : outOfRangeAimColor;
			Debug.DrawLine(playerPos, playerPos + (mousePosToPlayerDirection.normalized * mousePosToPlayerDistance), debugAimColor);
		}
	}

	public void grappleBeamWindingCheck(Vector2 playerPos) {
		//If the previous winding point has LOS, restore grapple beam joint

		if (grappleBeamWindPoints.Count > 0) {
			//Unwind the grapple beam from surfaces
			unwindGrappleBeamCheck(playerPos);

			//Wind the grapple beam around walls and surfaces
			windGrappleBeamCheck(playerPos);
		}
	}

	public bool unwindGrappleBeamCheck(Vector2 playerPos) {
		if (grappleBeamWindPoints.Count > 1) {
			//Check if the current grapple point has LOS.

			//Check the previous grapple point for LOS
			Vector2 previousGrapplePoint = grappleBeamWindPoints[grappleBeamWindPoints.Count-2];
			Vector2 checkDirection = previousGrapplePoint - playerPos;
			RaycastHit2D hit = Physics2D.Raycast(playerPos, checkDirection, checkDirection.magnitude - grappleBeamWindCheckLengthDecrease, grappleMask);

			//If the winding dot is no, the beam is rotating away from the normal of its current position
			float windingDot = evaluateWindingDot(playerPos);

			//Beam LOS is Restored! Unwind the grapple beam to the previous point.
			if(hit.collider == null && windingDot <= 0) {
				grappleBeamWindPoints.RemoveAt(grappleBeamWindPoints.Count-1);
				grappleBeamWindPointNormals.RemoveAt(grappleBeamWindPointNormals.Count-1);
				grappleJoint.connectedAnchor = previousGrapplePoint;
				grappleJoint.distance = (playerPos - previousGrapplePoint).magnitude;
				return true;
			}
			//Beam LOS is blocked. No changes necessary.
			else {
				//Do nothing.
				return false;
			}
		}
		return false;
	}

	public float evaluateWindingDot(Vector2 playerPos) {
		//Evaluate Windings
		Vector2 previousGrapplePoint = grappleBeamWindPoints[grappleBeamWindPoints.Count-2];
		Vector2 currentGrapplePoint = grappleBeamWindPoints[grappleBeamWindPoints.Count-1];
		Vector2 prevCur = (previousGrapplePoint - currentGrapplePoint);
		//Vector2 curPlayer = (currentGrapplePoint - playerPos);
		Vector3 prevPlayer = (previousGrapplePoint - playerPos);

		//Get the direction the beam should be winding away from
		//(Note that this vector can be zero or positive) 
		Vector2 bendNormal = Vector3.Cross(prevCur, Vector3.forward);
		float prevNormalDot = Vector2.Dot(bendNormal, prevPlayer);
		bendNormal *= ((prevNormalDot < 0) ? -1 : 1);
		Debug.DrawLine(currentGrapplePoint, currentGrapplePoint + bendNormal * 10, Color.magenta);

		Vector2 currentGrapplePointNormal = grappleBeamWindPointNormals[grappleBeamWindPointNormals.Count-1];
		float windDot = Vector2.Dot(currentGrapplePointNormal, bendNormal);
		Debug.DrawLine(currentGrapplePoint, currentGrapplePoint + currentGrapplePointNormal * 5, windDot > 0 ? Color.red : Color.yellow);

		return windDot;
		/*
		//Other
		float evalDot = Vector2.Dot(bendNormal.normalized, curPlayer.normalized);
		Vector2 evalNormal = (prevCur + curPlayer).normalized * ((evalDot < 0) ? -1 : 1);
		Debug.DrawLine(currentGrapplePoint, currentGrapplePoint + evalNormal * 10, Color.gray);

		Vector2 crossNormal = Vector3.Cross(evalNormal, Vector3.forward);
		float crossDot = Vector2.Dot(bendNormal, crossNormal);
		crossNormal *= ((crossDot < 0) ? -1 : 1);

		Color unwindingColor = (true) ? Color.white : Color.red;
		Debug.DrawLine(currentGrapplePoint, currentGrapplePoint + crossNormal * 10, unwindingColor);

		return Vector2.Dot(crossNormal, prevCur);
		*/
	}

	public bool windGrappleBeamCheck(Vector2 playerPos) {
		//If Grapple Beam LOS is broken, set the grapple beam's new anchor to point where LOS was broken
		Vector2 checkDirection = grappleBeamWindPoints[grappleBeamWindPoints.Count-1] - playerPos;
		RaycastHit2D hit = Physics2D.Raycast(playerPos, checkDirection, checkDirection.magnitude - grappleBeamWindCheckLengthDecrease, grappleMask);

		//Beam LOS was lost! A wind point should be created
		if (hit.collider != null) {
			grappleBeamWindPoints.Add(hit.point);
			grappleBeamWindPointNormals.Add(hit.normal);
			grappleJoint.connectedAnchor = hit.point;
			grappleJoint.distance = (playerPos - hit.point).magnitude;
			return true;
		}
		//Beam LOS is unblocked. No changes necessary
		else {
			//Do nothing.
			return false;
		}
	}

	public void applyGravityBeamMomentumForce() {
		Vector2 playerPos = player.transform.position;
		Vector2 gravityBeamVelocityDirection = (grappleJoint.connectedAnchor - playerPos).normalized;
		player.velocity = gravityBeamVelocityDirection * gravityBeamRetractSpeed;
		Debug.DrawLine(playerPos, playerPos + gravityBeamVelocityDirection * 4, Color.magenta, 1);
	}

	public void fireGrappleBeam(GrappleBeamType beamType) {
		//If already grappled, release the grapple only.
		if (grappled) {
			releaseGrapple();
			return;
		}

		Vector2 playerPos2D = player.transform.position;
		Vector2 mousePos2D = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);

		//Fire Grapple Beam
		Vector2 fireDirection = mousePos2D - playerPos2D;
		float beamRange = getGrappleBeamRange(beamType);
		RaycastHit2D hit = Physics2D.Raycast(playerPos2D, fireDirection.normalized, beamRange, grappleMask);

		//Grapple Beam hit something!
		if(hit.collider != null) {
			attachGrapple(hit.point, hit.distance, beamType);
		}
		//Nothing was hit
		else {
			Debug.DrawLine(playerPos2D, playerPos2D + ((mousePos2D - playerPos2D).normalized * beamRange), Color.yellow, 0.1f);
			releaseGrapple();
		}
	}

	public IEnumerator fireGrappleBeamProjectile() {
		grappleProjectileObject.SetActive(true);
		
		float distance = 0;
		float maxDistance = grappleBeamRange;
		float distanceSpeed = 20;
		while(distance < maxDistance) {
			distance += Time.deltaTime * distanceSpeed;
			distance = Mathf.Min(distance, maxDistance);

			Vector2 playerPos2D = player.transform.position;
			Vector2 mousePos2D = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 fireDirection = mousePos2D - playerPos2D;
			grappleProjectileObject.transform.position = playerPos2D + fireDirection.normalized * distance;

			yield return new WaitForEndOfFrame();
		}

		grappleProjectileObject.SetActive(false);
	}

	public void showGrappleAimLine(Vector2 playerPos, Vector2 mousePosition) {
		Vector2 mousePosToPlayerDirection = mousePosition - playerPos;
		float mousePosToPlayerDistance = Mathf.Min(mousePosToPlayerDirection.magnitude, grappleBeamRange);
		Color debugAimColor = mousePosToPlayerDistance < grappleBeamRange ? aimColor : outOfRangeAimColor;
		Debug.DrawLine(playerPos, playerPos + (mousePosToPlayerDirection.normalized * mousePosToPlayerDistance), debugAimColor);
	}

	public float getGrappleBeamRange(GrappleBeamType beamType) {
		switch(beamType) {
			case GrappleBeamType.Grapple:
				return grappleBeamRange;

			case GrappleBeamType.Gravity:
				return gravityBeamRange;

			default:
				return 10;
		}
	}

	public Color getGrappleBeamColor(GrappleBeamType beamType) {
		switch(beamType) {
			case GrappleBeamType.Grapple:
				return grappleBeamColor;

			case GrappleBeamType.Gravity:
				return gravityBeamColor;

			default:
				return Color.white;
		}
	}

	public void attachGrapple(Vector2 grapplePoint, float grappleLength, GrappleBeamType beamType) {
		grappleJoint.connectedAnchor = grapplePoint;
		grappleJoint.distance = grappleLength;
		grappled = true;
		grappleJoint.enabled = true;
		grappleBeamCurType = beamType;
		grappleBeamWindPoints.Add(grapplePoint);
	}

	public void releaseGrapple() {
		grappled = false;
		grappleJoint.enabled = false;
		grappleJoint.connectedAnchor = Vector2.zero;
		grappleBeamCurType = GrappleBeamType.Grapple;
		grappleBeamWindPoints.Clear();
		grappleBeamWindPointNormals.Clear();
	}
}
