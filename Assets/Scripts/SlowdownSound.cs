﻿using UnityEngine;
using System.Collections;

public class SlowdownSound : MonoBehaviour {

	private AudioSource audio;

	void Awake() {
		audio = GetComponent<AudioSource>();
	}

	void Update() {
		audio.pitch = Time.timeScale;
	}

}
