﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Rigidbody2D player;
	public Vector2 maxRange;

	void Update () {
		float cameraX = Mathf.Clamp(player.transform.position.x, maxRange.x, maxRange.y);
		transform.position = new Vector3(cameraX, transform.position.y, transform.position.z);
	}
}
