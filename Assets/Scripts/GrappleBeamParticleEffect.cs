﻿using UnityEngine;
using System.Collections.Generic;

public class GrappleBeamParticleEffect : MonoBehaviour {

	public GrappleBeam grappleBeam;
	public ParticleSystem grappleParticles;
	public ParticleSystem grappleAnchorEffect;
	public float grappleBeamDensity = 1;
	public float grappleBeamSegmentSize = 0.1f;

	public Color[] grappleBeamColors = {Color.green, Color.green};
	public Color[] gravityBeamColors = {Color.blue, Color.blue};

	public LineRenderer grappleBeamLine;

	private ParticleSystem.Particle[] grappleGeneratedParticles;
	private ParticleSystem.Particle[] noParticles = { };

	void Update () {
		if(grappleBeam.grappled) {
			showGrappleBeamParticles();
		}
		else {
			grappleParticles.SetParticles(noParticles, 0);
		}

		//Anchor Effect
		if(grappleAnchorEffect != null) {
			grappleAnchorEffect.enableEmission = grappleBeam.grappled;
			if(grappleBeam.grappleJoint.enabled)
				grappleAnchorEffect.transform.position = grappleBeam.grappleJoint.connectedAnchor;
		}

		//Update Grapple Beam Line
		int windingPoints = grappleBeam.grappleBeamWindPoints.Count;

		if (windingPoints > 1 && grappleBeamLine != null) {
			grappleBeamLine.SetVertexCount(windingPoints);
			grappleBeamLine.SetPosition(0, grappleBeam.grappleBeamWindPoints[0]);
			for (int i = 1; i < windingPoints; i++) {
				//Draw a line between the winding points
				if(i < windingPoints)
					grappleBeamLine.SetPosition(i, grappleBeam.grappleBeamWindPoints[i]);
			}
		}
		else {
			if(grappleBeamLine != null)
				grappleBeamLine.SetVertexCount(0);
		}
	}

	public void showGrappleBeamParticles() {
		var beam = grappleBeam.grappleJoint;
		Vector3 beamStartPosition = grappleBeam.playerCon.animationController.beamPosition.position;//grappleBeam.player.transform.position;
		float grappleBeamLength = beam.distance;
		float tetherSegments = Mathf.Ceil(grappleBeamLength * grappleBeamDensity);

		List<ParticleSystem.Particle> particles = new List<ParticleSystem.Particle>();
		for (int i = 0; i <= (int)tetherSegments; i++) {
			float percentage = i / tetherSegments;
			ParticleSystem.Particle p = new ParticleSystem.Particle();

			p.position = Vector3.Lerp(beamStartPosition, beam.connectedAnchor, percentage);
			if(grappleBeam.grappleBeamCurType == GrappleBeamType.Grapple)
				p.color = Color.Lerp(grappleBeamColors[0], grappleBeamColors[1], percentage);
			else
				p.color = Color.Lerp(gravityBeamColors[0], gravityBeamColors[1], percentage);
			p.size = grappleBeamSegmentSize;

			particles.Add(p);
		}

		//Set the Particles
		grappleGeneratedParticles = particles.ToArray();
		grappleParticles.SetParticles(grappleGeneratedParticles, grappleGeneratedParticles.Length);
	}


}
